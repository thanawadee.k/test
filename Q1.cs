﻿using System;
using System.Collections.Generic;

namespace Test
{
    public class Q1
    {
        public static void Main(string[] args)
        {
            Random rnd = new Random();
            int[,] card = new int[5, 5];
            List<int> numbers = new List<int>();
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    int randomNumber = rnd.Next(1, 25);
                    if (!numbers.Contains(randomNumber))
                    {
                        card[i, j] = randomNumber;
                        numbers.Add(randomNumber);
                    }
                    else
                    {

                        while (numbers.Contains(randomNumber))
                        {
                            randomNumber = rnd.Next(1, 25);
                        }
                        card[i, j] = randomNumber;
                        numbers.Add(randomNumber);
                    }
                }
            }

            string s = Console.ReadLine();
            int[] nums = Array.ConvertAll(s.Split(','), int.Parse);

            Console.WriteLine(winCheck(nums, card));
        }

        public static bool winCheck(int[] bingo, int[,] card)
        {
            int countHorizontal = 0;
            int countVertical = 0;
            int countDiagonal = 0;
            for (int row = 0; row < card.GetLength(0); row++)
            {
       
                countHorizontal = 0;

                for (int col = 0; col < bingo.Length; col++)
                {
                    Console.WriteLine(card[row, col]);
                    if (card[row, col] == bingo[col])
                    {
                        if (row == col)
                        {
                            countDiagonal++;
                        }
                        countHorizontal++;
                    }
                    if (countHorizontal == 3)
                    {
                        return true;
                    }
                    
                }
                if (countVertical == bingo.Length || countDiagonal == bingo.Length)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
